import mysql.connector
from datetime import datetime
from datetime import date


class info2mysql:
    def crawler2insert(database_name, table_name, id, post_date, title, subtitle, db_http_imgs_list, content, content_type, url):
        mydb = mysql.connector.connect(
            host="127.0.0.1",
            port="3306",
            user="root",
            password="Cx@123456",
            database=database_name
        )

        mycursor = mydb.cursor()
        mycursor.execute(f"CREATE TABLE IF NOT EXISTS {table_name}(id bigint(20), date date, title varchar(255), image1 varchar(255), created_at timestamp, tw_title varchar(255), en_title varchar(255), cn_title varchar(255), tw_subtitle varchar(255), en_subtitle varchar(255), cn_subtitle varchar(255), tw_content	text, en_content text, cn_content text, type varchar(255), enable tinyint(2), crawl_url varchar(255))")

        # get_current_date_n_time
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        today = date.today()
        now_date = today.strftime("%Y-%m-%d")
        current_data_n_time = f"{now_date} {current_time}"

        sql = "INSERT INTO {} (id, date, title, image1, created_at, tw_title, en_title, cn_title, tw_subtitle, en_subtitle, cn_subtitle, tw_content, en_content, cn_content, type, enable, crawl_url) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)".format(table_name)
        if len(db_http_imgs_list) < 1:
            val = (id, post_date, title, '', current_data_n_time, title, title, title,
                   subtitle, subtitle, subtitle, content, content, content, content_type, 0, url)
        else:
            val = (id, post_date, title, db_http_imgs_list[0], current_data_n_time, title, title,
                   title, subtitle, subtitle, subtitle, content, content, content, content_type, 0, url)
        mycursor.execute(sql, val)
        mydb.commit()

        print(current_data_n_time)

    def get_table_list(database_name, table_name, collumm_name):
        _list = []
        try:
            mydb = mysql.connector.connect(
                host="127.0.0.1",
                port="3306",
                user="root",
                password="Cx@123456",
                database=database_name
            )
            mycursor = mydb.cursor()

            mycursor.execute(f"select {collumm_name} from {table_name}")

            for i in mycursor:
                # print(i)
                # print(str(i[0]))
                _list.append(str(i[0]))
        except Exception as e:
            # print(e)
            pass

        return _list

    def initDB(database_name, table_name):
        mydb = mysql.connector.connect(
            host="127.0.0.1",
            port="3306",
            user="root",
            password="Cx@123456",
            database=database_name
        )
        mycursor = mydb.cursor()
        mycursor.execute(
            "CREATE DATABASE IF NOT EXISTS {} DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci".format(database_name))

        mydb = mysql.connector.connect(
            host="127.0.0.1",
            port="3306",
            user="root",
            password="Cx@123456",
            database=database_name
        )
        mycursor = mydb.cursor()
        mycursor.execute(
            f"CREATE TABLE IF NOT EXISTS {table_name}(id bigint(20), date date, title varchar(255), image1 varchar(255), created_at timestamp, tw_title varchar(255), en_title varchar(255), cn_title varchar(255), tw_subtitle varchar(255), en_subtitle varchar(255), cn_subtitle varchar(255), tw_content	text, en_content text, cn_content text, type varchar(255), enable tinyint(2), crawl_url varchar(255))")
