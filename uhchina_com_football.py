from os import makedirs
from os.path import exists
import functools
import signal
import sys
from bs4 import BeautifulSoup
import requests
from txt import txt
from time import sleep
from datetime import datetime
from datetime import date
from info2mysql import info2mysql
from download import download
from upload2tencent import upload2tencent
requests.packages.urllib3.disable_warnings()


# 定义一个超时错误类


class TimeoutError(Exception):
    pass


def time_out(seconds, error_msg='TIME_OUT_ERROR:No connection were found in limited time!'):
    # 带参数的装饰器
    def decorated(func):
        result = ''
        # 信号机制的回调函数，signal_num即为信号，frame为被信号中断那一时刻的栈帧

        def signal_handler(signal_num, frame):
            global result
            result = error_msg
            # raise显式地引发异常。一旦执行了raise语句，raise后面的语句将不能执行
            raise TimeoutError(error_msg)

        def wrapper(*args, **kwargs):
            global result
            signal.signal(signal.SIGALRM, signal_handler)
            # 如果time是非0，这个函数则响应一个SIGALRM信号并在time秒后发送到该进程。
            signal.alarm(seconds)
            try:
                # 若超时，此时alarm会发送信息激活回调函数signal_handler，从而引发异常终止掉try的代码块
                result = func(*args, **kwargs)
            finally:
                # 假如在callback函数未执行的时候，要取消的话，那么可以使用alarm(0)来取消调用该回调函数
                signal.alarm(0)
                print('finish')
                return result
        # return wrapper
        return functools.wraps(func)(wrapper)
    return decorated


class crawler:

    def get_soup(url):
        sleep(2)
        while True:
            try:
                resp = requests.get(url)
                soup = BeautifulSoup(resp.text, "lxml")
                return soup
                break
            except Exception as e:
                # print(e)
                sleep(60)
                pass

    # @time_out(21600)
    def main(crawler_item):
        url = r'https://uhchina.com/shipin/zuqiujijin/'
        soup = crawler.get_soup(url)
        items = soup.select('div.list_right.zuqiu_category_4 a')
        for item_url in items:
            if item_url.text != r'足球集锦':
                if item_url.text == crawler_item:
                    print(item_url.text)
                    url = r'https://uhchina.com' + item_url['href']
                    print(url)
                    page_num = crawler.get_page_max_num(url)
                    print(r'page_num: ' + str(page_num))
                    crawler.get_page_links(item_url.text, url, page_num)

    def get_page_max_num(url):
        soup = crawler.get_soup(url)
        pages = soup.select('div.pagesbox')
        if len(pages) > 0:
            links = pages[0].select('a')
            pages_list = []
            x = 1
            while x < len(links)-1:
                pages_list.append(int(links[x].text))
                x += 1
            if len(pages_list) == 0:
                pages_list.append(2)
            return max(pages_list)

        else:
            return 1

    def get_page_links(item_name, url, page_max_num):
        for num in range(1, page_max_num+1):
            _url = f"{url}{str(num)}.htm"
            print(_url)

            soup = crawler.get_soup(_url)

            if len(soup.select('.bofang3box')) > 0:
                article_urls = soup.select('.bofang3box .tvtitle3 a')
                for article_url in article_urls:
                    print(article_url.text.strip())
                    print(r'https://uhchina.com' + article_url['href'])
                    # print('====================================')
                    crawler.check_repeat_url(
                        item_name, r'https://uhchina.com' + article_url['href'])
            '''
            if len(soup.select('.box_1')) > 0 :
                article_urls = soup.select('.bofang3box .tvtitle3 a')
                for article_url in article_urls:
                    print(article_url.text.strip())
                    print(r'https://uhchina.com' + article_url['href'])
                #print('====================================')
                    crawler.check_repeat_url(item_name, r'https://uhchina.com' + article_url['href'])

            elif len(soup.select('.shipinbox4'))> 0 :
                article_urls = soup.select('.bofang3box .tvtitle3 a')
                for article_url in article_urls:
                    print(article_url.text.strip())
                    print(r'https://uhchina.com' + article_url['href'])
                #print('====================================')            
                    crawler.check_repeat_url(item_name, r'https://uhchina.com' + article_url['href'])
            '''

    def check_repeat_url(item_name, url):
        info2mysql.initDB(r'uhchina_com_football', item_name)
        db_url_list = info2mysql.get_table_list(
            r'uhchina_com_football', item_name, r'crawl_url')
        print(url)
        if len(db_url_list) > 0:
            # avoid crawl and insert again
            if not url in db_url_list:
                crawler.get_txt_content(item_name, url)
                db_url_list.append(url)
            else:
                print(r'Crawl Done !! Skip This Url: ' + url)
                # get_current_date_n_time
                now = datetime.now()
                current_time = now.strftime("%H:%M:%S")
                today = date.today()
                now_date = today.strftime("%Y-%m-%d")
                current_data_n_time = f"{now_date} {current_time}"
                print(current_data_n_time)
                sleep(0.5)
        else:
            crawler.get_txt_content(item_name, url)
            db_url_list.append(url)

    @time_out(21600)
    def get_txt_content(item_name, url):
        try:
            print('This is Page Url ----------------------------')
            print(url)
            print('This is Page Url ----------------------------')
            info2mysql.initDB(r'uhchina_com_football', item_name)
            id_list = info2mysql.get_table_list(
                'uhchina_com_football', item_name, r'id')
            if len(id_list) > 0:
                num_list = []
                for id in id_list:
                    num_list.append(int(id))
                id = int(max(num_list)) + 1
            else:
                id = 1

            soup = crawler.get_soup(url)
            artcontent = soup.select('.showbox')

            post_date = artcontent[0].select('span')
            post_date = post_date[1].text
            post_date = txt.remove_date(post_date)
            print(post_date)

            title = artcontent[0].select('h1')
            title = title[0].text
            print(title)

            video_n_img_url_list = []
            if len(artcontent[0].select('iframe')) > 0:
                video = artcontent[0].select('iframe')
                video_n_img_url_list.append(video[0]['src'])
                print(video[0]['src'])

            if len(artcontent[0].select('img')) > 0:
                imgs = artcontent[0].select('img')
                for img in imgs:
                    if r'http' in img['src']:
                        video_n_img_url_list.append(img['src'])
                        print(img['src'])
                    else:
                        print(r'https://uhchina.com' + img['src'])
                        video_n_img_url_list.append(
                            r'https://uhchina.com' + img['src'])
            save_folder = r'/www/wwwroot/data/images/football'
            if not exists(save_folder):
                makedirs(save_folder)
            img_n_video_list = download.img_n_video(
                save_folder, title, video_n_img_url_list)

            txt_contents = artcontent[0].select('.introduction')
            _content = ""
            for txt_content in txt_contents:
                txt_content = txt_content.text
                txt_content = txt_content.strip()
                txt_content = txt_content + '\n'
                _content += txt_content

            if len(img_n_video_list) < 1:
                print(f"This Url Can't Download Images n Video: {url}")

            elif len(img_n_video_list) > 0:
                for url_dict in img_n_video_list:
                    for _url in video_n_img_url_list:
                        if r'.mp4' in url_dict[_url]:
                            video_path = url_dict[_url]
                            upload_result = upload2tencent.video(video_path)
                            _content = _content + '\n' + str(upload_result)

            under_contents = soup.select('.jieshao')
            if len(under_contents) > 0:
                under_contents = under_contents[0].text
                _content = _content + '\n' + under_contents

            print(
                'THis is Content ------------------------------------------------------')
            print(_content)
            print(
                'THis is Content ------------------------------------------------------')

            info2mysql.crawler2insert(r'uhchina_com_football', item_name,
                                      id, post_date, title, r'', r'', _content, item_name, url)
        except Exception as err:
            print(err)
            print(f'This Url Can Not Crawl!!: {url}')
            pass


if __name__ == '__main__':
    while True:
        try:
            input_list = sys.argv
            crawler.main(str(input_list[1]))

            '''  
            crawler.main(r'梅西')
            crawler.main(r'C罗')
            crawler.main(r'托雷斯')
            crawler.main(r'卡卡')
            crawler.main(r'范佩西')
            crawler.main(r'鲁尼')
            crawler.main(r'比利亚')
            crawler.main(r'杰拉德')
            crawler.main(r'小罗')
            crawler.main(r'德罗巴')
            crawler.main(r'贝克汉姆')
            crawler.main(r'亨利')
            crawler.main(r'罗本')
            crawler.main(r'纳尼')
            crawler.main(r'罗纳尔多')
            crawler.main(r'巴洛特利')
            crawler.main(r'香川真司')
            crawler.main(r'伊布')
            crawler.main(r'戈麦斯')
            crawler.main(r'齐达内')
            crawler.main(r'马拉多纳')
            crawler.main(r'博格坎普')
            crawler.main(r'伊斯科')
            crawler.main(r'埃托奥')
            crawler.main(r'登巴巴')
            crawler.main(r'卢卡库')
            crawler.main(r'罗比尼奥')
            
            crawler.main(r'五佳球')
            crawler.main(r'十佳球')
            crawler.main(r'过人集锦')
            crawler.main(r'任意球集锦')
            crawler.main(r'头球集锦')
            crawler.main(r'角球集锦')
            crawler.main(r'点球集锦')
            crawler.main(r'助攻集锦')
            crawler.main(r'倒勾集锦')
            crawler.main(r'远射集锦')
            crawler.main(r'进球集锦')

            crawler.main(r'足球录像')
            crawler.main(r'天下足球')
            crawler.main(r'街头足球')
            crawler.main(r'足球宝贝')
            crawler.main(r'搞笑视频')
            crawler.main(r'打架视频')
            '''
        except Exception as err:
            print(err)
            pass
