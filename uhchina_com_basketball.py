from os import makedirs
from os.path import exists
import functools
import signal
import sys
from bs4 import BeautifulSoup
import requests
from txt import txt
from time import sleep
from datetime import datetime
from datetime import date
import re
from info2mysql import info2mysql
import requests.packages.urllib3
from upload2tencent import upload2tencent
from download import download
requests.packages.urllib3.disable_warnings()

# 定义一个超时错误类


class TimeoutError(Exception):
    pass


def time_out(seconds, error_msg='TIME_OUT_ERROR:No connection were found in limited time!'):
    # 带参数的装饰器
    def decorated(func):
        result = ''
        # 信号机制的回调函数，signal_num即为信号，frame为被信号中断那一时刻的栈帧

        def signal_handler(signal_num, frame):
            global result
            result = error_msg
            # raise显式地引发异常。一旦执行了raise语句，raise后面的语句将不能执行
            raise TimeoutError(error_msg)

        def wrapper(*args, **kwargs):
            global result
            signal.signal(signal.SIGALRM, signal_handler)
            # 如果time是非0，这个函数则响应一个SIGALRM信号并在time秒后发送到该进程。
            signal.alarm(seconds)
            try:
                # 若超时，此时alarm会发送信息激活回调函数signal_handler，从而引发异常终止掉try的代码块
                result = func(*args, **kwargs)
            finally:
                # 假如在callback函数未执行的时候，要取消的话，那么可以使用alarm(0)来取消调用该回调函数
                signal.alarm(0)
                print('finish')
                return result
        # return wrapper
        return functools.wraps(func)(wrapper)
    return decorated


class crawler:
    def get_soup(url):
        sleep(0.5)
        while True:
            try:
                resp = requests.get(url)
                soup = BeautifulSoup(resp.text, "lxml")
                return soup
                break
            except Exception as e:
                # print(e)
                sleep(60)
                pass

    def main(crawler_item):
        nba_not_crawler_list = [
            'NBA视频集锦',
            'NBA赛程',
            'NBA排名榜',
            'NBA直播吧',
            'CBA赛程',
            'NBA球队名称',
            'CBA直播吧',
            '2019-2020',
            '2018-2019',
            '2017-2018',
            '2016-2017',
            '2015-2016',
            '2014-2015',
            '2013-2014',
            '2012-2013',
            '2011-2012',
            '2010-2011'
        ]
        url = r'https://uhchina.com/nbashipin/luxiang/'
        soup = crawler.get_soup(url)
        items = soup.select('div .left_a a')
        for item_url in items:
            if not item_url.text in nba_not_crawler_list:
                if item_url.text == crawler_item:
                    print(item_url.text)
                    url = r'https://uhchina.com' + item_url['href']
                    print(url)
                    page_num = crawler.get_page_max_num(url)
                    print(page_num)
                    crawler.get_page_url(item_url.text, url, page_num)

    def get_page_max_num(url):
        soup = crawler.get_soup(url)
        pages = soup.select('div.pagesbox')
        if len(pages) > 0:
            links = pages[0].select('a')
            pages_list = []
            x = 1
            while x < len(links)-1:
                pages_list.append(int(links[x].text))
                x += 1
            if len(pages_list) == 0:
                pages_list.append(2)
            return max(pages_list)
        else:
            return 1

    def get_page_url(item_name, url, page_max_num):
        if r'.htm' in url:
            for num in range(1, page_max_num + 1):
                _url = re.sub(r'.htm', f"_{str(num)}.htm", url)
                print('=================This is Page Url======================')
                print(_url)
                print('=================This is Page Url======================')
                crawler.get_article_url(item_name, _url)
        else:
            for num in range(1, page_max_num + 1):
                _url = f"{url}{str(num)}.htm"
                print('=================This is Page Url======================')
                print(_url)
                print('=================This is Page Url======================')
                crawler.get_article_url(item_name, _url)

    def get_article_url(item_name, url):
        soup = crawler.get_soup(url)
        if len(soup.select('.box_1  a')) > 0:
            items = soup.select('.box_1  a')

        elif len(soup.select('.bofang3box a')) > 0:
            items = soup.select('.bofang3box .tvtitle3 a')

        elif len(soup.select('.shipinbox4')) > 0:
            items = soup.select('.shipinbox4 .tvtitle4 a')

        for article_url in items:
            print(article_url.text)
            print(r'https://uhchina.com' + article_url['href'])
            crawler.check_repeat_url(
                item_name, r'https://uhchina.com' + article_url['href'])

    def check_repeat_url(item_name, url):
        info2mysql.initDB(r'uhchina_com_basketball', item_name)
        db_url_list = info2mysql.get_table_list(
            r'uhchina_com_basketball', item_name, r'crawl_url')
        print(url)
        if len(db_url_list) > 0:
            # avoid crawl and insert again
            if not url in db_url_list:
                crawler.get_txt_content(item_name, url)
                db_url_list.append(url)
            else:
                print(r'Crawl Done !! Skip This Url: ' + url)
                # get_current_date_n_time
                now = datetime.now()
                current_time = now.strftime("%H:%M:%S")
                today = date.today()
                now_date = today.strftime("%Y-%m-%d")
                current_data_n_time = f"{now_date} {current_time}"
                print(current_data_n_time)
                sleep(0.5)
        else:
            crawler.get_txt_content(item_name, url)
            db_url_list.append(url)

    @time_out(14400)
    def get_txt_content(item_name, url):
        try:
            print('This is Page Url ----------------------------')
            print(url)
            print('This is Page Url ----------------------------')
            info2mysql.initDB(r'uhchina_com_basketball', item_name)
            id_list = info2mysql.get_table_list(
                r'uhchina_com_basketball', item_name, r'id')
            if len(id_list) > 0:
                num_list = []
                for id in id_list:
                    num_list.append(int(id))
                id = int(max(num_list)) + 1
            else:
                id = 1

            soup = crawler.get_soup(url)
            artcontent = soup.select('.showbox.border')

            post_date = artcontent[0].select('span')
            post_date = post_date[1].text
            post_date = txt.remove_date(post_date)
            print(post_date)

            title = artcontent[0].select('h1')
            title = title[0].text
            print(title)

            video_n_img_url_list = []
            if len(artcontent[0].select('iframe')) > 0:
                video = artcontent[0].select('iframe')
                video_n_img_url_list.append(video[0]['src'])
                # print(video[0]['src'])

            if len(artcontent[0].select('img')) > 0:
                imgs = artcontent[0].select('img')
                for img in imgs:
                    if r'http' in img['src']:
                        video_n_img_url_list.append(img['src'])
                        print(img['src'])
                    else:
                        print(r'https://uhchina.com' + img['src'])
                        video_n_img_url_list.append(
                            r'https://uhchina.com' + img['src'])

            save_folder = r'/www/wwwroot/data/images/basketball'
            if not exists(save_folder):
                makedirs(save_folder)
            img_n_video_list = download.img_n_video(
                save_folder, title, video_n_img_url_list)

            txt_contents = artcontent[0].select('.introduction')
            _content = ""
            for txt_content in txt_contents:
                txt_content = txt_content.text
                txt_content = txt_content.strip()
                txt_content = txt_content + '\n'
                _content += txt_content

            if len(img_n_video_list) < 1:
                print(f"This Url Can't Download Images n Video: {url}")

            elif len(img_n_video_list) > 0:
                for url_dict in img_n_video_list:
                    for _url in video_n_img_url_list:
                        print(_url)
                        if r'.mp4' in url_dict[_url]:
                            video_path = url_dict[_url]
                            upload_result = upload2tencent.video(video_path)
                            _content = _content + '\n' + str(upload_result)
                        else:
                            http_img_path = url_dict[_url]
                            print(http_img_path)
                            _content = _content + '\n' + str(http_img_path)

            under_contents = soup.select('.jieshao')
            if len(under_contents) > 0:
                under_contents = under_contents[0].text
                _content = _content + '\n' + under_contents

            print(
                'THis is Content ------------------------------------------------------')
            print(_content)
            print(
                'THis is Content ------------------------------------------------------')
            info2mysql.initDB(r'uhchina_com_basketball', item_name)
            info2mysql.crawler2insert(r'uhchina_com_basketball', item_name,
                                      id, post_date, title, r'', r'', _content, item_name, url)
        except Exception as err:
            print(err)
            print(f'This Url Can Not Crawl!!: {url}')
            pass


if __name__ == '__main__':
    while True:
        try:

            input_list = sys.argv
            crawler.main(str(input_list[1]))

            '''
            crawler.main(r'奥尼尔')
            crawler.main(r'朗多')
            crawler.main(r'加内特')
            crawler.main(r'JR史密斯')
            crawler.main(r'纳什')
            crawler.main(r'邓肯')
            crawler.main(r'皮尔斯')
            crawler.main(r'雷阿伦')
            crawler.main(r'帕克')
            crawler.main(r'德隆')
            crawler.main(r'克劳福德')
            crawler.main(r'诺维斯基')
            crawler.main(r'易建联')
            crawler.main(r'基德')
            crawler.main(r'吉诺比利')
            crawler.main(r'波什')
            crawler.main(r'NBA五佳球')
            crawler.main(r'NBA十佳球')
            crawler.main(r'抢断集锦')
            crawler.main(r'扣篮集锦')
            crawler.main(r'盖帽集锦')
            crawler.main(r'助攻集锦')
            crawler.main(r'打架视频')
            crawler.main(r'过人集锦')
            crawler.main(r'篮板集锦')
            crawler.main(r'三分球集锦')
            crawler.main(r'科比')
            crawler.main(r'麦蒂')
            crawler.main(r'詹姆斯')
            crawler.main(r'韦德')
            crawler.main(r'艾弗森')
            crawler.main(r'乔丹')
            crawler.main(r'林书豪')
            crawler.main(r'保罗')
            crawler.main(r'卡特')
            crawler.main(r'罗斯')
            crawler.main(r'杜兰特')
            crawler.main(r'霍华德')
            crawler.main(r'安东尼')
            crawler.main(r'姚明')

            crawler.main(r'湖人')
            crawler.main(r'凯尔特人')
            crawler.main(r'热火')
            crawler.main(r'篮网')
            crawler.main(r'尼克斯')
            crawler.main(r'魔术')
            crawler.main(r'奇才')
            crawler.main(r'活塞')
            crawler.main(r'步行者')
            crawler.main(r'鹈鹕')
            crawler.main(r'雄鹿')
            crawler.main(r'老鹰')            
            crawler.main(r'公牛')
            crawler.main(r'猛龙')
            crawler.main(r'骑士')
            crawler.main(r'独行侠')
            crawler.main(r'马刺')
            crawler.main(r'森林狼')
            crawler.main(r'爵士')

            crawler.main(r'NBA季前赛录像')
            crawler.main(r'NBA常规赛录像')
            crawler.main(r'NBA季后赛录像')
            crawler.main(r'NBA总决赛录像')
            crawler.main(r'NBA夏季联赛录像')
            '''
            pass
        except Exception as err:
            print(err)
            pass
