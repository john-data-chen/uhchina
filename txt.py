import re

class txt:
    #uhchina.com qtx.com
    def remove_html_tag(txt):
        txt = re.sub(r'[<]\salt[=]["].*src[=]["]|"/>', '\n', txt)
        txt = txt.split(r'</script>')
        txt = re.sub(r'<script.*[>]|[$][(].*', '', txt[0])
        txt = re.sub(r'<[^>]+>', '', txt)
        return txt
    
    #dianjinghu.com
    def remove_html(txt):
        if r'alt' in txt:
            txt = re.sub(r'[<]\salt="[^>]+data-original[=]|["][/][>]', '\n', txt)
        elif r'class' in txt:
            txt = re.sub(r'[<]\sclass[=]["]lazy["]\sdata-original|"\s.*[<][/]p[>]', '\n', txt)
        txt = re.sub(r'<[^>]+>', '', txt)
        txt = re.sub(r'style="display:block; margin-left:auto; margin-right:auto;.*"', '', txt)
        txt = re.sub(r'style=".*', '', txt)
        txt = re.sub(r'["][/][>]', '', txt)
        txt = re.sub(r'["][/]', r'https://lol.dianjinghu.com/', txt)
        txt = re.sub(r'="', '', txt)
        txt = re.sub(r'=http', r'http', txt)
        return txt

    def remove_date(txt):
        txt = re.sub(r'来源.*', '', txt)
        txt = re.sub(r'.*视频更新：|NBA直播吧.*', '', txt)
        txt = re.sub(r'发布在NBA |&nbsp&nbsp&nbsp.*', '', txt)
        txt = re.sub(r'发布在CBA |&nbsp&nbsp&nbsp.*', '', txt)
        txt = re.sub(r'..:..|日' ,r'', txt)
        txt = re.sub(r'年|月', r'-', txt)
        return txt

    #hupu.com
    def remove_html_hupu(txt):
        txt = re.sub(r'["][/][>]', r'<', txt)
        txt = re.sub(r'src="', r'>', txt)
        txt = re.sub(r'<[^>]+>', '', txt)
        return txt

    def remove_keyword(txt):
        txt = re.sub(r'球天下', '', txt)
        txt = re.sub(r'[(]编辑：姚凡[)]|[(]编辑：Mask[)]', '', txt)
        return txt

    def title_remove(txt):
        txt = re.sub(r'[/]', r'_', txt)
        return txt

    def items_link(txt):
        txt = re.sub(r'title=.*', '', txt)
        txt = re.sub(r'[^"]+[<]|[<][/]span[>]|[/]div[>]', '', txt)
        txt = re.sub(r'[^"]+=', '', txt)
        txt = re.sub(r'"', '', txt)
        return txt
    
    def save(content, title, path):
        with open(path + r'/' + title + r'.txt', 'a', encoding="utf-8") as f:
            f.write(content)
            f.write('\n')
    
    def get_http_path(img_donwload_path):
        http_path = re.sub(r'/www/wwwroot/data/', r'http://news.shzd08.com/', img_donwload_path)
        return http_path

    #for dianjinghu.com use
    def http2https(url):
        orginal_urls = []
        http_url = re.sub(r'https:', r'http:', url)
        http_www_url = re.sub(r'lol.dianjinghu.com', r'www.dianjinghu.com', http_url)
        http_lol_url = re.sub(r'www.dianjinghu.com', r'lol.dianjinghu.com', http_url)

        https_url = re.sub(r'http:', r'https:', url)
        https_www_url = re.sub(r'lol.dianjinghu.com', r'www.dianjinghu.com', https_url)
        https_lol_url = re.sub(r'www.dianjinghu.com', r'lol.dianjinghu.com', https_url)

        orginal_urls.append(http_www_url)
        orginal_urls.append(http_lol_url)
        orginal_urls.append(https_www_url)
        orginal_urls.append(https_lol_url)

        return orginal_urls

    def http_img_special(url):
        url = re.sub(r'[?]', r'[?]', url)
        url = re.sub(r'&height', r'&amp;height', url)
        return url
    
    def txt_content_change_img_url(db_img_path_dict, http_imgs_list, _content):
        for http_img in http_imgs_list:
            #print(http_img)
            orginal_url = db_img_path_dict[http_img]
            if r'dianjinghu.com' in orginal_url:
                orginal_urls = txt.http2https(str(orginal_url))
                for orginal_url in orginal_urls:
                    orginal_url = txt.http_img_special(orginal_url)
                    #print(orginal_url) 
                    _content = re.sub(orginal_url, f'<img src="{http_img}">', _content)
            else:
                orginal_url = str(orginal_url)
                orginal_url = txt.http_img_special(orginal_url)
                #print(orginal_url) 
                _content = re.sub(orginal_url, f'<img src="{http_img}">', _content)
            _content = re.sub(r'" title="', '', _content)
        return _content


