from os import makedirs
from os.path import exists
import functools
import signal
from bs4 import BeautifulSoup
import re
import requests
from txt import txt
from time import sleep
from datetime import datetime
from datetime import date
from info2mysql import info2mysql
from download import download
from upload2tencent import upload2tencent
import sys
requests.packages.urllib3.disable_warnings()

# 定义一个超时错误类


class TimeoutError(Exception):
    pass


def time_out(seconds, error_msg='TIME_OUT_ERROR:No connection were found in limited time!'):
    # 带参数的装饰器
    def decorated(func):
        result = ''
        # 信号机制的回调函数，signal_num即为信号，frame为被信号中断那一时刻的栈帧

        def signal_handler(signal_num, frame):
            global result
            result = error_msg
            # raise显式地引发异常。一旦执行了raise语句，raise后面的语句将不能执行
            raise TimeoutError(error_msg)

        def wrapper(*args, **kwargs):
            global result
            signal.signal(signal.SIGALRM, signal_handler)
            # 如果time是非0，这个函数则响应一个SIGALRM信号并在time秒后发送到该进程。
            signal.alarm(seconds)
            try:
                # 若超时，此时alarm会发送信息激活回调函数signal_handler，从而引发异常终止掉try的代码块
                result = func(*args, **kwargs)
            finally:
                # 假如在callback函数未执行的时候，要取消的话，那么可以使用alarm(0)来取消调用该回调函数
                signal.alarm(0)
                print('finish')
                return result
        # return wrapper
        return functools.wraps(func)(wrapper)
    return decorated


class crawler:
    def main(crawl_item):
        url = r'https://uhchina.com/shipin/zuqiujijin/'
        soup = crawler.get_soup(url)
        items = soup.select('div.shipin_league')
        items = str(items[0]).split(r'</span>')
        for item in items:
            url = re.sub(
                r'.*href="/|/" title.*|<span>[|]|<div class="shipin_league">|</div>', '', item)
            url = f"https://uhchina.com/{url.strip()}/"
            item_name = re.sub(r'.*">|</a>.*|<span>[|]|</div>', '', item)
            if crawl_item in item_name:
                page_num = crawler.get_page_max_num(url)
                crawler.get_all_url(url, item_name, page_num)

    def get_soup(url):
        sleep(2)
        while True:
            try:
                resp = requests.get(url)
                soup = BeautifulSoup(resp.text, "lxml")
                return soup
                break
            except Exception as e:
                # print(e)
                sleep(60)
                pass

    def get_page_max_num(url):
        soup = crawler.get_soup(url)
        # print(soup)
        pages = soup.select('div.pagesbox')
        if len(pages) > 0:
            links = pages[0].select('a')
            pages_list = []
            x = 1
            while x < len(links)-1:
                pages_list.append(int(links[x].text))
                x += 1
            # print(max(pages_list))
            return max(pages_list)

    def get_all_url(item_url, item_name, page_num):
        info2mysql.initDB(r'uhchina_com_football_league', item_name)
        db_url_list = info2mysql.get_table_list(
            r'uhchina_com_football_league', item_name, r'crawl_url')
        for i in range(2, page_num+1):
            page_url = f"{item_url}{str(i)}.htm"
            soup = crawler.get_soup(page_url)
            art_area = soup.select('div.ppdweqwe.clearfix')
            links = art_area[0].select('a')
            for link in links:
                art_url = f"https://uhchina.com{link['href']}"
                # print(art_url)
                if len(db_url_list) > 0:
                    # avoid crawl and insert again
                    if not art_url in db_url_list:
                        crawler.get_txt_content(item_name, art_url)
                        db_url_list.append(art_url)
                    else:
                        print(r'Crawl Done !! Skip This Url: ' + art_url)
                        # get_current_date_n_time
                        now = datetime.now()
                        current_time = now.strftime("%H:%M:%S")
                        today = date.today()
                        now_date = today.strftime("%Y-%m-%d")
                        current_data_n_time = f"{now_date} {current_time}"
                        print(current_data_n_time)
                        sleep(0.5)
                else:
                    crawler.get_txt_content(item_name, art_url)
                    db_url_list.append(art_url)

    @time_out(14400)
    def get_txt_content(item_name, url):
        try:
            print('This is Page Url ----------------------------')
            print(url)
            print('This is Page Url ----------------------------')
            info2mysql.initDB(r'uhchina_com_football_league', item_name)
            id_list = info2mysql.get_table_list(
                'uhchina_com_football_league', item_name, r'id')
            if len(id_list) > 0:
                num_list = []
                for id in id_list:
                    num_list.append(int(id))
                id = int(max(num_list)) + 1
            else:
                id = 1

            soup = crawler.get_soup(url)
            artcontent = soup.select('.showbox')

            post_date = artcontent[0].select('span')
            post_date = post_date[1].text
            post_date = txt.remove_date(post_date)
            print(post_date)

            title = artcontent[0].select('h1')
            title = title[0].text
            print(title)

            video_n_img_url_list = []
            if len(artcontent[0].select('iframe')) > 0:
                video = artcontent[0].select('iframe')
                video_n_img_url_list.append(video[0]['src'])
                print(video[0]['src'])

            if len(artcontent[0].select('img')) > 0:
                imgs = artcontent[0].select('img')
                for img in imgs:
                    if r'http' in img['src']:
                        video_n_img_url_list.append(img['src'])
                        print(img['src'])
                    else:
                        print(r'https://uhchina.com' + img['src'])
                        video_n_img_url_list.append(
                            r'https://uhchina.com' + img['src'])
            save_folder = r'/www/wwwroot/data/images/football'
            if not exists(save_folder):
                makedirs(save_folder)
            img_n_video_list = download.img_n_video(
                save_folder, title, video_n_img_url_list)

            txt_contents = artcontent[0].select('.introduction')
            _content = ""
            for txt_content in txt_contents:
                txt_content = txt_content.text
                txt_content = txt_content.strip()
                txt_content = txt_content + '\n'
                _content += txt_content

            if len(img_n_video_list) < 1:
                print(f"This Url Can't Download Images n Video: {url}")

            elif len(img_n_video_list) > 0:
                for url_dict in img_n_video_list:
                    for _url in video_n_img_url_list:
                        if r'.mp4' in url_dict[_url]:
                            video_path = url_dict[_url]
                            upload_result = upload2tencent.video(video_path)
                            _content = _content + '\n' + str(upload_result)

            under_contents = soup.select('.jieshao')
            if len(under_contents) > 0:
                under_contents = under_contents[0].text
                _content = _content + '\n' + under_contents

            print(
                'THis is Content ------------------------------------------------------')
            print(_content)
            print(
                'THis is Content ------------------------------------------------------')

            info2mysql.crawler2insert(r'uhchina_com_football_league', item_name,
                                      id, post_date, title, r'', r'', _content, item_name, url)
        except Exception as err:
            print(err)
            print(f'This Url Can Not Crawl!!: {url}')
            pass


if __name__ == '__main__':

    # 英超|意甲|西甲|德甲|欧冠杯|中超|法甲|亚冠杯
    while True:
        try:

            input_list = sys.argv
            crawler.main(str(input_list[1]))

            '''
            crawler.main(r'英超')
            crawler.main(r'意甲')
            crawler.main(r'西甲')
            crawler.main(r'德甲')
            crawler.main(r'欧冠杯')
            crawler.main(r'中超')
            crawler.main(r'法甲')
            crawler.main(r'亚冠杯')
            '''
        except Exception as err:
            print(err)
            pass
